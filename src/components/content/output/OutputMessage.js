import { Component } from "react";

import likeImg from "../../../assets/images/like.png";

class OutputMessage extends Component {
    render() {
        return (
            <div>
                <div className="row mt-4">
                    <div className="col-12">
                        <p>Thông điệp ở đây</p>
                    </div>
                </div>
                <div className="row mt-2">
                    <div className="col-12">
                        <img src={likeImg} alt="like" width={100}/>
                    </div>
                </div>
            </div>
        )
    }
}

export default OutputMessage;