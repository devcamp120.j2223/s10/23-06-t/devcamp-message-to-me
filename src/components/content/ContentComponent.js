import { Component } from "react";

import InputMessage from "./input/InputMessage";
import OutputMessage from "./output/OutputMessage";

class ContentComponent extends Component {
    render() {
        return (
            <div>
                <InputMessage />
                <OutputMessage />
            </div>
        )
    }
}

export default ContentComponent;